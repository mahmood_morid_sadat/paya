<?php

namespace Mahmood\Paya;

use Illuminate\Support\ServiceProvider;

class PayaServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([__DIR__.'/config/holidays.php'=>config_path('holidays.php')],'config');
    }
}
