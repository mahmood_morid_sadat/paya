# Paya Package Example

## Installation:

- Add below lines to your composer.json file:

~~~
    "repositories": [
        {
            "type": "vcs",
            "url": "https://gitlab.com/mahmood_morid_sadat/paya.git"
        }
    ],
~~~
- Also add package name to require part:
~~~
"require": {
...
        "mahmood/paya": "*"
~~~

- Then run this command to install the package:

~~~
composer update  
~~~


## Usage:

~~~php
use Mahmood\Paya\paya\dto\GetDateDto;
use Mahmood\Paya\paya\PaymentDate;

// Get paya date time
        $getDateDto = new GetDateDto();
        $getDateDto->date = '2022-03-21 18:00:00';
        $getDateDto->bank = 'mellat' //Or you can feel it empty.
        $getPaymentDate = new PaymentDate($getDateDto);
        $result = $getPaymentDate->getPayDate();

// $result: instance of  Mahmood\Paya\paya\dto\TimeDto;
// $result->time: "2022-03-26 04:00:00",
// $result->bank: "mellat",
// $result->message: "Paya payment time calculated."


// Get next working date time
        $getDateDto = new GetDateDto();
        $getDateDto->date = '2022-07-09 16:37:04';
        $getPaymentDate = new PaymentDate($getDateDto);
        $result = $getPaymentDate->getNextWorkingDay();

// $result: instance of  Mahmood\Paya\paya\dto\NextWorkingDayDTO;
// $result->time: "2022-07-11 16:00:00",
~~~
