<?php

namespace Mahmood\Paya\paya\dto;

class NextWorkingDayDTO
{
    public $time;
    public $message;
}