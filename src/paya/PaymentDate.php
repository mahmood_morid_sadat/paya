<?php

namespace Mahmood\Paya\paya;

use Carbon\Carbon;
use Mahmood\Paya\paya\dto\NextWorkingDayDTO;
use Mahmood\Paya\paya\dto\TimeDto;
use Mahmood\Paya\paya\enums\Banks;
use Mahmood\Paya\paya\dto\GetDateDto;

class PaymentDate
{
    private $getDateDto;
    private $timeDto;
    private $nextWorkingDayDTO;

    /**
     * @param GetDateDto $getDateDto
     */
    public function __construct(GetDateDto $getDateDto)
    {
        $this->getDateDto = $getDateDto;
        $this->timeDto = new TimeDto();
        $this->nextWorkingDayDTO = new NextWorkingDayDTO();
    }

    public function getNextWorkingDay() : NextWorkingDayDTO
    {

        $timeDto = $this->timeDto;
        $nextWorkingDayDTO = $this->nextWorkingDayDTO;
        $getDateDto = $this->getDateDto;
        $userDateObject = Carbon::createFromFormat('Y-m-d H:i:s', $getDateDto->date);
        $newUserHourObject = $this->findNotNextDayHoliday($userDateObject->format('Y-m-d'));
        $newUserHourObject->hour(16);
        $newUserHourObject->minute(0);
        $newUserHourObject->second(0);
        $nextWorkingDayDTO->time = $newUserHourObject->format('Y-m-d H:i:s');
        $nextWorkingDayDTO->message = 'زمان تایید پس از ۲۴ ساعت کاری';
        return $nextWorkingDayDTO;
    }

    public function getNextThreeWorkingDay() : NextWorkingDayDTO
    {
        $timeDto = $this->timeDto;
        $nextWorkingDayDTO = $this->nextWorkingDayDTO;
        $getDateDto = $this->getDateDto;
        $userDateObject = Carbon::createFromFormat('Y-m-d H:i:s', $getDateDto->date);
        $userDateObject->addDays(1);
        for ($i = 0 ; $i < 2 ;$i++)
        {
            $userDateObject = $this->findNotNextDayHoliday($userDateObject->format('Y-m-d'));
        }

        $userDateObject->hour(16);
        $userDateObject->minute(0);
        $userDateObject->second(0);
        $nextWorkingDayDTO->time = $userDateObject->format('Y-m-d H:i:s');
        $nextWorkingDayDTO->message = 'زمان تایید پس از سه روز کاری';
        return $nextWorkingDayDTO;
    }

    /**
     * @return TimeDto
     */
    public function getPayDate(): TimeDto
    {
        $timeDto = $this->timeDto;
        $getDateDto = $this->getDateDto;
        $userDateObject = Carbon::createFromFormat('Y-m-d H:i:s', $getDateDto->date);

        if (strcmp($getDateDto->bank, Banks::AYANDE) == 0) {
            $timeDto->bank = Banks::AYANDE;
            $userDateObject->addMinutes(15);
            $timeDto->time = $userDateObject->format('Y-m-d H:i:s');
            $timeDto->message = 'Ayande bank will receive in 15 minutes';
        } else {
            $newUserHourObject = $userDateObject;
            if (!$this->isHoliday($userDateObject->format('Y-m-d'))) {
                $userHour = $userDateObject->format('H');
                $userMinute = $userDateObject->format('i');
                switch (true) {
                    case 0 <= $userHour && $userHour <= 2:
                        $newUserHourObject->hour(4);
                        break;
                    case 3 <= $userHour &&
                        ($userHour < 10 || ($userHour == 10 && $userMinute < 30)):
                        $newUserHourObject->hour(12);
                        break;
                    case ($userHour == 10 && $userMinute >= 30) ||
                        ($userHour > 10 &&
                            ($userHour < 12 ||
                                ($userHour == 12 && $userMinute < 30))):
                        $newUserHourObject->hour(14);
                        break;
                    case ($userHour == 12 && $userMinute >= 30) ||
                        17 > $userHour ||
                        ($userHour == 17 && $userMinute < 30):
                        $newUserHourObject->hour(19);
                        break;
                    case ($userHour == 17 && $userMinute >= 30) ||
                        (17 < $userHour && $userHour <= 23):
                        $newUserHourObject = $this->findNotNextDayHoliday($newUserHourObject->format('Y-m-d'));
                        $newUserHourObject->hour(4);
                        break;
                }
            } else {
                $newUserHourObject = $this->findNotNextDayHoliday($newUserHourObject->format('Y-m-d'));
                $newUserHourObject->hour(4);
            }
            $newUserHourObject->minute(0);
            $newUserHourObject->second(0);
            $timeDto->bank = $getDateDto->bank;
            $timeDto->time = $newUserHourObject->format('Y-m-d H:i:s');
            $timeDto->message = 'Paya payment time calculated.';
        }
        return $timeDto;
    }

    public function isHoliday(string $date): bool
    {
        $holidays = config('holidays');
        $today = Carbon::createFromFormat('Y-m-d', $date);
        return in_array($date, $holidays) || $today->dayOfWeekIso == 5;
    }

    /**
     * @param string $date
     * @return Carbon
     */
    public function findNotNextDayHoliday(string $date): Carbon
    {
        $today = Carbon::createFromFormat('Y-m-d', $date);
        $tomorrow = $today->addDays(1);
        $holidays = config('holidays');
        do {
            if (in_array($tomorrow->format('Y-m-d'), $holidays)) {
                $found = true;
            } else {
                $found = false;
            }
            if ($found || $tomorrow->dayOfWeekIso == 5) {
                $isHoliday = true;
                $tomorrow = $tomorrow->addDays(1);
            } else {
                $isHoliday = false;
            }
        } while ($isHoliday == true);

        return $tomorrow;
    }
}
